# micro-service-archetype

### 介绍
基于maven的微服务骨架，用于快速生成微服务项目骨架，只需两条命令即可完成创建。

### 各分支简介
#### simple-mvc：生成一个基于spring-boot、spring-mvc的单体应用
#### simple-hessian：生成一个基于spring-boot的，使用hessian作为rpc的服务化应用
#### dubbo-zookeeper：生成一个基于spring-boot、dubbo的微服务应用

